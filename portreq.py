import requests
from requests.exceptions import HTTPError
host = input('Host: ')
ports = input("Ports: ")
##список портов через пробел
ports = ports.split(' ')
ports = list(ports)
useragent = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

## http
for i in ports:
    httpUrl = ('http://{}'.format(host))
    httpUrlPort = ('{}{}{}/'.format(httpUrl, ':', i))
    print(httpUrlPort)
    try:
        httpResponse = requests.get(httpUrlPort, headers=useragent, cookies={'JSESSIONID': 'null'}, timeout=10)
        print(httpResponse.status_code)
        print(httpResponse.json)
        print(httpResponse.headers)
        print(httpResponse.content)
    except requests.exceptions.ConnectTimeout:
        print('Connection timeout')
    except requests.exceptions.ConnectionError:
        print('DNS lookup failed')
    except requests.exceptions.HTTPError as err:
        print('HTTP Error occured')
        print('Response is: {content}'.format(content=err.response.content))
## https
for i in ports:
    httpsUrl = ('https://{}'.format(host))
    httpsUrlPort = ('{}{}{}/'.format(httpsUrl, ':', i))
    print(httpsUrlPort)
    try:
        httpsResponse = requests.get(httpsUrlPort, headers=useragent, cookies={'JSESSIONID': 'null'}, timeout=10)
        print(httpsResponse.status_code)
        print(httpsResponse.json)
        print(httpsResponse.headers)
        print(httpsResponse.content)
    except requests.exceptions.ConnectTimeout:
        print('Connection timeout')
    except requests.exceptions.ConnectionError:
        print('DNS lookup failed')
    except requests.exceptions.HTTPError as err:
        print('HTTP Error occured')
        print('Response is: {content}'.format(content=err.response.content))
